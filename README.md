> ## 🛠 Status: In Development
> Happy Tracker is currently in development. 

# Happy Tracker

This a student tracking app for use in refugee camps.
gives you the following features:
- all the PWA goodness (manifest, service worker)
- offline functionality for camps with bad connectivity
- offline UI
- a responsive layout
- using Redux for state management
- simple routing solution
- fast time-to-interactive and first-paint through the PRPL pattern
- unit and integrating testing

### Getting started
Please ensure that you have Google Cloud SDK installed, logged in and selected a project.

Building and deploying is very simple:
```
npm install
npm run build
npm run deploy
```
For local development, `npm serve` can be used to serve the built files,
or use `npm start` to see your changes updated immediately.