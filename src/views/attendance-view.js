import { html } from 'lit-element'
import { PageViewElement } from './page-view-element'

// These are the shared styles needed by this element.
import { SharedStyles } from '../components/shared-styles.js';

class AttendanceView extends PageViewElement {
  static get styles() {
    return [SharedStyles]
  }

  render () {
    return html`
      <section>
        <h2>Welcome to HappyTracker</h2>
        <p>HappyTracker keeps track of your students, so you have less to worry.</p>
        <p>
            Have you found anything that could be better or is not working as intended?<br \>
            Contact me on WhatsApp at (+31) 6 57568946 or on email at matthijs.kok2@gmail.com
        </p>
      </section>
    `
  }
}

window.customElements.define('attendance-view', AttendanceView)