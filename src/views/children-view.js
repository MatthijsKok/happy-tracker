import { html } from 'lit-element'
import { PageViewElement } from './page-view-element'

import '../components/class/class-list'

import { store } from '../store.js';
import children from '../reducers/children.js';
store.addReducers({
  children
});

// These are the shared styles needed by this element.
import { SharedStyles } from '../components/shared-styles.js';

class ChildrenView extends PageViewElement {
  static get styles() {
    return [SharedStyles]
  }

  render () {
    return html`
      <section>
        <h2>Children Management</h2>
        <p>HappyTracker keeps track of your students, so you have less to worry.</p>
        <class-list></class-list>
        
      </section>
    `
  }
}

window.customElements.define('children-view', ChildrenView)
