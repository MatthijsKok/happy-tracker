import { TODO, AARGH } from '../actions/children'

const INITIAL_STATE = {
  classes: [
    {
      id: 1,
      name: 'Blue Alpha',
      children: [
        'aad', 'bert', 'charlie'
      ]
    },
    {
      id: 2,
      name: 'Green Bravo',
      children: [
        'david', 'eduard', 'frank'
      ]
    },
    {
      id: 3,
      name: 'Red Tango',
      children: [
        'G', 'herman', 'iota'
      ]
    },
  ]
};


const children = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TODO:
      return {
        ...state,
        todo: true
      }
    case AARGH:
      return {
        ...state,
        aargh: true
      }
    default:
      return state
  }
}

export default children;