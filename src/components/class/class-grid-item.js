import { LitElement, html, css } from 'lit-element'

class ClassGridItem extends LitElement {
  static get properties() {
    return {
      child: { type: Object }
    }
  }

  static get styles() {
    return [
      css`
        .card {
          background: #fff;
          border-radius: 5px;
          display: inline-block;
          /* height: 80px; */
          width: 162px;
          margin: 0.25rem;
          padding: 0.5rem;
          position: relative;
          box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        }
        
        .card p {
          text-align: center
        }
        
        vaadin-accordion-panel {
          
        }
      `
    ]
  }


  render () {
    return html`
      <div class="card">
        <p>${this.child}</p>
      </div>
    `
  }
}

window.customElements.define('class-grid-item', ClassGridItem)