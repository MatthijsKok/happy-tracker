import { LitElement, html } from 'lit-element';

import './class-grid-item'
import { repeat } from 'lit-html/directives/repeat'

class ClassGrid extends LitElement {
  static get properties() {
    return {
      children: { type: Array }
    }
  }

  render () {
    return html`
        <a>Children:</a><br \>
        ${repeat(this.children, child => html`
          <class-grid-item .child="${child}"></class-grid-item>
        `)}
    `
  }
}

window.customElements.define('class-grid', ClassGrid)