import { LitElement, html } from 'lit-element';

import './class-grid'

class ClassListItem extends LitElement {
  static get properties() {
    return {
      item: { type: Object }
    }
  }

  render () {
    return html`
      <a>Class: ${this.item.name}</a><br \>
      <class-grid .children="${this.item.children}"></class-grid>
    `
  }
}

window.customElements.define('class-list-item', ClassListItem)