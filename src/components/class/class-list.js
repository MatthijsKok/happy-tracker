import { LitElement, html, css } from 'lit-element'
import { repeat } from 'lit-html/directives/repeat'
import { connect } from 'pwa-helpers/connect-mixin'
import '@vaadin/vaadin-accordion'

import { store } from '../../store'
import { SharedStyles } from '../shared-styles.js';
import './class-list-item'

class ClassListElement extends connect(store)(LitElement) {
  static get properties() {
    return {
      _classes: { type: Array },
      _total: { type: Number }
    };
  }

  static get styles() {
    return [
      SharedStyles,
      css`
        vaadin-accordion-panel {
          
        }
      `
    ]
  }

  render() {
    return html`
        <h2>Class List</h2>
        <p ?hidden="${this._classes.length !== 0}">There are no classes for you to display.</p>
        <vaadin-accordion>
        ${repeat(this._classes,  item => html`
          <vaadin-accordion-panel theme="filled reverse">
            <div slot="summary">${item.name}</div>
            <class-list-item .item="${item}"></class-list-item>
          </vaadin-accordion-panel>
        `)}
        </vaadin-accordion>

    `
  }

  // constructor() {
  //   super();
  //   this._classes = [
  //     {
  //       "id": 1,
  //       "name": "Blue Alpha"
  //     },
  //     {
  //       "id": 2,
  //       "name": "Green Bravo"
  //     },
  //     {
  //       "id": 3,
  //       "name": "Red Tango"
  //     },
  //   ]
  // }

  stateChanged(state) {
    this._classes = state.children.classes;
  }
}

window.customElements.define('class-list', ClassListElement)
